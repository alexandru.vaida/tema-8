<?php

require('transport.class.php');

class plane extends transport{
    private $tip;
    private $locuri;

    public function __construct($tip,$locuri){
        $this->tip = $tip;
        $this->locuri = $locuri;
    }

    public function setTip($tip) {
        $this->tip = $tip;
    }

    public function getTip() {
        return $this->tip;
    }
   
   
    public function setLocuri($locuri) {
        $this->locuri = $locuri;
    }

    public function getLocuri() {
        return $this->locuri;
    }

}



?>
