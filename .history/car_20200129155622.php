<?php
 
 require('vehicle.class.php');

 class car extends vehicle {
     private $tip;
     private $combustibil;
     
     public function __construct($tip, $combustibil ){
        $this->tip = $tip;
        $this->combustibil = $combustibil;
    }

    public function setTip($valoare) {
        $this->tip = $valoare;
    }

     public function getTip() {
            return $this->tip;
        }

        public function setCombustibil($valoare) {
            $this->combustibil = $valoare;
        }
        
         public function getCombustibil() {
                return $this->combustibil;
            }
       

 }

?>