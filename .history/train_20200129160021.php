<?php

require('transport.class.php');

class plane extends transport{
    private $tip;
    private $numarVagoane;

    public function __construct($tip,$numarVagoane){
        $this->tip = $tip;
        $this->numarVagoane = $numarVagoane;
    }

    public function setTip($valoare) {
        $this->tip = $valoare;
    }

    public function getTip() {
        return $this->tip;
    }
   
   
    public function setNumarVagoane($valoare) {
        $this->numarVagoane = $valoare;
    }

    public function getNumarVagoane() {
        return $this->numarVagoane;
    }

}



?>
