<?php

require('transport.class.php');

class plane extends transport{
    private $tip;
    private $numarVagoane;

    public function __construct($tip,$numarVagoane){
        $this->tip = $tip;
        $this->numarVagoane = $numarVagoane;
    }

    public function setTip($tip) {
        $this->tip = $tip;
    }

    public function getTip() {
        return $this->tip;
    }
   
   
    public function setNumarVagoane($numarVagoane) {
        $this->numarVagoane = $numarVagoane;
    }

    public function getNumarVagoane() {
        return $this->numarVagoane;
    }

}



?>
