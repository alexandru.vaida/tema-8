<?php

require('transport.class.php');

class plane extends transport{
    private $tip;
    private $locuri;

    public function __construct($tip,$locuri){
        $this->tip = $tip;
        $this->locuri = $locuri;
    }

    public function setTip($valoare) {
        $this->tip = $valoare;
    }

    public function getTip() {
        return $this->tip;
    }
   
   
    public function setLocuri($valoare) {
        $this->locuri = $valoare;
    }

    public function getLocuri() {
        return $this->locuri;
    }

}



?>
