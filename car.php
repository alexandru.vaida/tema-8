<?php
 
 require('vehicle.class.php');

 class car extends vehicle {
     private $tip;
     private $combustibil;
     
     public function __construct($tip, $combustibil ){
        $this->tip = $tip;
        $this->combustibil = $combustibil;
    }

    public function setTip($tip) {
        $this->tip = $tip;
    }

     public function getTip() {
            return $this->tip;
        }

        public function setCombustibil($combustibil) {
            $this->combustibil = $combustibil;
        }
        
         public function getCombustibil() {
                return $this->combustibil;
            }
       

 }

?>